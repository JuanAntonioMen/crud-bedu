[[productos]]
== Producto

=== Delete elimina  producto
Elimina un producto

==== Ejemplo de petición
include::{snippets}/producto/delete-producto/http-request.adoc[]

==== Ejemplo de respuesta
include::{snippets}/producto/delete-producto/http-response.adoc[]

==== Ejemplo usando CURL
include::{snippets}/producto/delete-producto/curl-request.adoc[]