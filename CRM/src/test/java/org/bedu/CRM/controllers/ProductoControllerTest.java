package org.bedu.CRM.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.bedu.CRM.model.Cliente;
import org.bedu.CRM.model.Producto;
import org.bedu.CRM.services.ProductoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import javax.validation.constraints.PastOrPresent;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureRestDocs
@WebMvcTest(ProductoController.class)
class ProductoControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private ProductoService productoService;

    @Test
    void getProducto() throws Exception {
        LocalDate fechaCreacion =  LocalDate.now();
        String fecha = String.valueOf(fechaCreacion);
        given(productoService.obtenProducto(anyLong())).willReturn(Optional.of(Producto.builder().id(1L).nombre("Nombre").categoria("Categoria").precio((float) 1.1).numeroRegistro("111 222 3333").fechaCreacion(fechaCreacion).build()));

        mockMvc.perform(get("/producto/{productoId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nombre", is("Nombre")))
                .andExpect(jsonPath("$.categoria", is("Categoria")))
                .andExpect(jsonPath("$.precio", is(1.1)))
                .andExpect(jsonPath("$.numeroRegistro", is("111 222 3333")))
                .andExpect(jsonPath("$.fechaCreacion", is(fecha)))

                .andDo(document("producto/get-producto",
                        pathParameters(
                                parameterWithName("productoId").description("Identificador del producto")
                        ),
                        responseFields(
                                fieldWithPath("id").description("identificador del producto"),
                                fieldWithPath("nombre").description("nombre del cliente"),
                                fieldWithPath("categoria").description("categoria del producto"),
                                fieldWithPath("precio").description("precio del producto"),
                                fieldWithPath("numeroRegistro").description("numero del registro del producto"),
                                fieldWithPath("fechaCreacion").description("fecha creacion del producto")
                        )));
    }

    @Test
    void getProductos() throws Exception {
        LocalDate fechaCreacion =  LocalDate.now();
        List<Producto> productos = Arrays.asList(
                Producto.builder().id(1L).nombre("Nombre 1").categoria("Categoria 1").precio(10).numeroRegistro("111 222 3333").fechaCreacion(fechaCreacion).build()
        );

        given(productoService.obtenProductos()).willReturn(productos);

        mockMvc.perform(get("/producto")
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nombre", is("Nombre 1")))
                .andExpect(jsonPath("$[0].categoria", is("Categoria 1")))
                .andExpect(jsonPath("$[0].precio", is(10.0)))
                .andExpect(jsonPath("$[0].numeroRegistro", is("111 222 3333")))
                .andExpect(jsonPath("$[0].fechaCreacion", is(fechaCreacion.toString())))

                .andDo(document("producto/get-productos",
                        responseFields(
                                fieldWithPath("[].id").description("identificador del producto"),
                                fieldWithPath("[].nombre").description("nombre del producto"),
                                fieldWithPath("[].categoria").description("categoria del producto"),
                                fieldWithPath("[].precio").description("precio del producto"),
                                fieldWithPath("[].numeroRegistro").description("numero de registro del producto"),
                                fieldWithPath("[].fechaCreacion").description("fecha creacion del producto")
                        )));
    }

/*    @Test
    void creaProducto() throws Exception {
        LocalDate fechaCreacion =  LocalDate.now();
        Producto productoParametro = Producto.builder().nombre("Nombre").categoria("Categoria").precio(10.0F).numeroRegistro("111 22 4444").build();
        Producto productoRespuesta = Producto.builder().id(1L).nombre("Nombre").categoria("Categoria").precio(10.0F).numeroRegistro("111 22 4444").build();

        given(productoService.guardaProducto(productoParametro)).willReturn(productoRespuesta);

        mockMvc.perform(post("/producto")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(productoParametro)))
                .andExpect(status().isCreated())

                .andDo(document("producto/post-producto",
                        requestFields(
                                fieldWithPath(".id").description("identificador del producto"),
                                fieldWithPath(".nombre").description("nombre del producto"),
                                fieldWithPath(".categoria").description("categoria del producto"),
                                fieldWithPath(".precio").description("precio del producto"),
                                fieldWithPath(".numeroRegistro").description("numero de registro del producto"),
                                fieldWithPath(".fechaCreacion").description("fecha creacion del producto")
                        ),
                        responseHeaders(
                                headerWithName("Location").description("La ubicación del recurso (su identificador generado")
                        ))
                );
    }*/

    @Test
    void eliminaProducto() throws Exception {
        mockMvc.perform(delete("/producto/{productoId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())

                .andDo(document("producto/delete-producto",
                        pathParameters(
                                parameterWithName("productoId").description("Identificador del producto")
                        )));
    }
}