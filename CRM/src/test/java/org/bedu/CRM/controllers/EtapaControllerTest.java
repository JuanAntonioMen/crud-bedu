package org.bedu.CRM.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bedu.CRM.model.Cliente;
import org.bedu.CRM.model.Etapa;
import org.bedu.CRM.services.EtapaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@AutoConfigureRestDocs
@WebMvcTest(EtapaController.class)
class EtapaControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EtapaService etapaService;

    @Test
    void getEtapa() throws Exception {
        given(etapaService.obtenEtapa(anyLong())).willReturn(Optional.of(Etapa.builder().etapaId(1L).nombre("Nombre").orden(1).build()));

        mockMvc.perform(get("/etapa/{etapaId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))

                .andExpect(jsonPath("$.etapaId", is(1)))
                .andExpect(jsonPath("$.nombre", is("Nombre")))
                .andExpect(jsonPath("$.orden", is(1)))

                .andDo(document("etapa/get-etapa",
                        pathParameters(
                                parameterWithName("etapaId").description("Identificador de la Etapa")
                        ),
                        responseFields(
                                fieldWithPath("etapaId").description("identificador de la etapa"),
                                fieldWithPath("nombre").description("nombre de la etapa"),
                                fieldWithPath("orden").description("número de orden")
                        )));
    }


/*    @Test
    void actualizaEtapa() throws Exception {

        Etapa etapaParametro = Etapa.builder().etapaId(1L).nombre("Nombre").orden(10).build();

        mockMvc.perform(put("/etapa/{etapaId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(etapaParametro)))
                .andExpect(status().isNoContent())

                .andDo(document("etapa/put-etapa",
                        pathParameters(
                                parameterWithName("etapaId").description("Identificador de la etapa")
                        ),
                        requestFields(
                                fieldWithPath("etapaId").description("El identificador del nuevo cliente"),
                                fieldWithPath("nombre").description("El nombre de l etapa"),
                                fieldWithPath("orden").description("La orden de la etapa")
                        )
                ));
    }*/

    @Test
    void eliminaEtapa() throws Exception {
        mockMvc.perform(delete("/etapa/{etapaId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())

                .andDo(document("etapa/delete-etapa",
                        pathParameters(
                                parameterWithName("etapaId").description("Identificador de la etapa")
                        )));
    }
}// Fin clase Test