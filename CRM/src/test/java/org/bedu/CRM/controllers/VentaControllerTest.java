package org.bedu.CRM.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bedu.CRM.model.Cliente;
import org.bedu.CRM.model.Producto;
import org.bedu.CRM.model.Venta;
import org.bedu.CRM.services.VentaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureRestDocs
@WebMvcTest(VentaController.class)
class VentaControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VentaService ventaService;

    @Test
    void getVenta() throws Exception {
        given(ventaService.obtenVenta(anyLong())).willReturn(Optional.of(Venta.builder().ventaId(1L).monto(1.0F).build()));

        mockMvc.perform(get("/venta/{ventaId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.ventaId", is(1)))
                .andExpect(jsonPath("$.monto", is(1.0)))

                .andDo(document("venta/get-venta",
                        pathParameters(
                                parameterWithName("ventaId").description("Identificador de la venta")
                        ),
                        responseFields(
                                fieldWithPath("ventaId").description("identificador de la venta"),
                                fieldWithPath("monto").description("monto de la venta"),
                                fieldWithPath("productos").description("productos de la venta"),
                                fieldWithPath("cliente").description("cliente de la venta"),
                                fieldWithPath("fechaCreacion").description("fecha de la venta")
                        )));
    }

/*    @Test
    void getVentas() throws Exception {

        List<Venta> ventas = Arrays.asList(
                Venta.builder().ventaId(1L).monto((float) 1.1).build(),
                Venta.builder().ventaId(2L).monto((float) 2.2).build(),
                Venta.builder().ventaId(3L).monto((float) 3.3).build()
        );

        given(ventaService.obtenVentas()).willReturn(ventas);

        mockMvc.perform(get("/venta")
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].ventaId", is(1)))
                .andExpect(jsonPath("$[0].monto", is(1.0)))

                .andDo(document("venta/get-ventas",
                        responseFields(
                                fieldWithPath("[].ventaId").description("identificador de la venta"),
                                fieldWithPath("[].monto").description("monto de la venta")
                        )));
    }*/

/*    @Test
    void creaVenta() throws Exception {
        List<Producto> productos = new ArrayList<>();
        Cliente clienteParametro = Cliente.builder().nombre("Nombre").direccion("Direccion").numeroEmpleados(String.valueOf(10)).correoContacto("contacto@cliente.com").build();
        Producto producto = Producto.builder().nombre("Nombre").categoria("Categoria").precio(1.0F).numeroRegistro("XXX XXX XXXX").build();
        productos.add(producto);
        Venta   ventaParametro = Venta.builder().ventaId(1L).monto(1.0F).productos(productos).cliente(clienteParametro).build();
        Venta   ventaRespuesta = Venta.builder().ventaId(1L).monto(1.0F).productos(productos).cliente(clienteParametro).build();

        given(ventaService.guardaVenta(ventaParametro)).willReturn(ventaRespuesta);

        mockMvc.perform(post("/venta")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(ventaParametro)))
                .andExpect(status().isCreated())

                .andDo(document("venta/post-venta",
                        requestFields(
                                fieldWithPath("ventaId").description("El identificador de la venta"),
                                fieldWithPath("monto").description("El nombre del cliente"),
                                fieldWithPath("productos").description("Lista de productos"),
                                fieldWithPath("cliente").description("La informacion del  cliente")
                        ),
                        responseHeaders(
                                headerWithName("Location").description("La ubicación del recurso (su identificador generado")
                        ))
                );
    }*/

    @Test
    void eliminaVenta() throws Exception {
        mockMvc.perform(delete("/venta/{ventaId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())

                .andDo(document("venta/delete-venta",
                        pathParameters(
                                parameterWithName("ventaId").description("Identificador de la venta")
                        )));
    }
}