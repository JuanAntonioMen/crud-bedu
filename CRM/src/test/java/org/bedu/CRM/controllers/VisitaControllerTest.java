package org.bedu.CRM.controllers;


import org.bedu.CRM.model.Cliente;
import org.bedu.CRM.model.Visita;
import org.bedu.CRM.services.VisitaService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.jupiter.api.Assertions.*;

@AutoConfigureRestDocs
@WebMvcTest(VisitaController.class)
class VisitaControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VisitaService visitaService;

/*    @Test
    void getVisita() throws Exception {

        Cliente cliente = Cliente.builder().nombre("Nombre").direccion("Direccion").numeroEmpleados(String.valueOf(10)).correoContacto("contacto@cliente.com").build();
        given(visitaService.obtenVisita(anyLong())).willReturn(Optional.of(Visita.builder().id(1L).cliente(cliente).direccion("Direccion").proposito("Proposito").vendedor("Vendedor").build()));

        mockMvc.perform(get("/visita/{visitaId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.cliente", is("Cliente")))
                .andExpect(jsonPath("$.direccion", is("Direccion")))
                .andExpect(jsonPath("$.proposito", is("Proposito")))
                .andExpect(jsonPath("$.vendedor", is("Vendedor")))
//                .andExpect(jsonPath("$.fechaProgramada", is(null)))

                .andDo(document("visita/get-visita",
                        pathParameters(
                                parameterWithName("visitaId").description("Identificador de la visita")
                        ),
                        responseFields(
                                fieldWithPath("id").description("identificador del cliente"),
                                fieldWithPath("cliente").description("datos del cliente"),
                                fieldWithPath("direccion").description("direccion de la visita"),
                                fieldWithPath("proposito").description("proposito de la visita"),
                                fieldWithPath("vendedor").description("vendedor de la visita")
                        )));
    }*/

    @Test
    void eliminaVisita() throws Exception {
        mockMvc.perform(delete("/visita/{visitaId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())

                .andDo(document("visita/delete-visita",
                        pathParameters(
                                parameterWithName("visitaId").description("Identificador de la visita")
                        )));
    }
}