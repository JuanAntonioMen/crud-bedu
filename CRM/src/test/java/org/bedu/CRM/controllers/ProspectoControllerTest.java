package org.bedu.CRM.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.bedu.CRM.model.Cliente;
import org.bedu.CRM.model.Prospecto;
import org.bedu.CRM.services.ProductoService;
import org.bedu.CRM.services.ProspectoService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.restdocs.AutoConfigureRestDocs;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.BDDMockito.given;
import static org.springframework.restdocs.headers.HeaderDocumentation.headerWithName;
import static org.springframework.restdocs.headers.HeaderDocumentation.responseHeaders;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.RestDocumentationRequestBuilders.*;
import static org.springframework.restdocs.payload.PayloadDocumentation.*;
import static org.springframework.restdocs.request.RequestDocumentation.parameterWithName;
import static org.springframework.restdocs.request.RequestDocumentation.pathParameters;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import static org.junit.jupiter.api.Assertions.*;


@AutoConfigureRestDocs
@WebMvcTest(ProspectoController.class)
class ProspectoControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProspectoService prospectoService;

    @Test
    void getProspecto() throws Exception {
        given(prospectoService.obtenProspecto(anyLong())).willReturn(Optional.of(Prospecto.builder().id(1L).nombre("Nombre").correoContacto("cliente@contacto.com").direccion("Direccion").build()));

        mockMvc.perform(get("/prospecto/{prospectoId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.nombre", is("Nombre")))
                .andExpect(jsonPath("$.correoContacto", is("cliente@contacto.com")))
                .andExpect(jsonPath("$.direccion", is("Direccion")))

                .andDo(document("prospecto/get-prospecto",
                        pathParameters(
                                parameterWithName("prospectoId").description("Identificador del prospecto")
                        ),
                        responseFields(
                                fieldWithPath("id").description("identificador del cliente"),
                                fieldWithPath("nombre").description("nombre del cliente"),
                                fieldWithPath("correoContacto").description("correo de contacto del cliente"),
                                fieldWithPath("direccion").description("domicilio del cliente")
                        )));
    }

    @Test
    void getProspectos() throws Exception {

        List<Prospecto> prospectos = Arrays.asList(
                Prospecto.builder().id(1L).nombre("Nombre 1").correoContacto("contacto@prospecto.com").direccion("Direccion").build()
        );

        given(prospectoService.obtenProspectos()).willReturn(prospectos);

        mockMvc.perform(get("/prospecto")
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].nombre", is("Nombre 1")))
                .andExpect(jsonPath("$[0].correoContacto", is("contacto@prospecto.com")))
                .andExpect(jsonPath("$[0].direccion", is("Direccion")))

                .andDo(document("prospecto/get-prospecto",
                        responseFields(
                                fieldWithPath("[].id").description("identificador del prospecto"),
                                fieldWithPath("[].nombre").description("nombre del prospecto"),
                                fieldWithPath("[].correoContacto").description("correo de contacto del prospecto"),
                                fieldWithPath("[].direccion").description("domicilio del prospecto")
                        )));
    }

    @Test
    void creaProspecto() throws Exception {
        Prospecto prospectoParametro = Prospecto.builder().nombre("Nombre").direccion("Direccion").correoContacto("contacto@prospecto.com").build();
        Prospecto prospectoRespuesta = Prospecto.builder().id(1L).nombre("Nombre").direccion("Direccion").correoContacto("contacto@prospecto").build();

        given(prospectoService.guardaProspecto(prospectoParametro)).willReturn(prospectoRespuesta);

        mockMvc.perform(post("/prospecto")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(prospectoParametro)))
                .andExpect(status().isCreated())

                .andDo(document("prospecto/post-prospecto",
                        requestFields(
                                fieldWithPath(".id").description("identificador del prospecto"),
                                fieldWithPath(".nombre").description("nombre del prospecto"),
                                fieldWithPath(".correoContacto").description("correo de contacto del prospecto"),
                                fieldWithPath(".direccion").description("domicilio del prospecto")
                        ),
                        responseHeaders(
                                headerWithName("Location").description("La ubicación del recurso (su identificador generado")
                        ))
                );
    }

    @Test
    void actualizaProspecto() throws Exception {

        Prospecto prospectoParametro = Prospecto.builder().id(1L).nombre("Nombre").direccion("Direccion").correoContacto("contacto@prospecto").build();

        mockMvc.perform(put("/prospecto/{prospectoId}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(new ObjectMapper().writeValueAsString(prospectoParametro)))
                .andExpect(status().isNoContent())

                .andDo(document("prospecto/put-prospecto",
                        pathParameters(
                                parameterWithName("prospectoId").description("Identificador del prospecto")
                        ),
                        requestFields(
                                fieldWithPath(".id").description("identificador del prospecto"),
                                fieldWithPath(".nombre").description("nombre del prospecto"),
                                fieldWithPath(".correoContacto").description("correo de contacto del prospecto"),
                                fieldWithPath(".direccion").description("domicilio del prospecto")
                        )
                ));
    }

    @Test
    void eliminaProspecto() throws Exception {
        mockMvc.perform(delete("/prospecto/{prospectoId}", 1)
                        .content(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isNoContent())

                .andDo(document("prospecto/delete-prospecto",
                        pathParameters(
                                parameterWithName("prospectoId").description("Identificador del prospecto")
                        )));
    }
}