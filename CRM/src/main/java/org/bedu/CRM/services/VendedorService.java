package org.bedu.CRM.services;

import lombok.RequiredArgsConstructor;
import org.bedu.CRM.controllers.mappers.VendedorMapper;
import org.bedu.CRM.model.Vendedor;
import org.bedu.CRM.persistence.VendedorRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class VendedorService {
    private final VendedorRepository repository;
    private final VendedorMapper mapper;

    public Vendedor guardaVendedor(Vendedor vendedor) {
        return mapper.vendedorEntityToVendedorModel(
                repository.save(mapper.vendedorModelToVendedorEntity(vendedor))
        );
    }

    public List<Vendedor> obtenVendedores(){
        return repository.findAll().stream().map(vendedor -> mapper.vendedorEntityToVendedorModel(vendedor)).collect(Collectors.toList());
    }

    public Optional<Vendedor> obtenVendedor(long idVendedor) {
        return repository.findById(idVendedor)
                .map(vendedor -> Optional.of(mapper.vendedorEntityToVendedorModel(vendedor)))
                .orElse(Optional.empty());
    }

    public void eliminaVendedor(long idvendedor){
        repository.deleteById(idvendedor);
    }

    public Vendedor actualizaVendedor(Vendedor vendedor){
        return mapper.vendedorEntityToVendedorModel(
                repository.save(mapper.vendedorModelToVendedorEntity(vendedor))
        );
    }

    public long cuenteVendedores(){
        return repository.count();
    }
}
