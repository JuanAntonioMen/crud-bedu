package org.bedu.CRM.services;

import lombok.RequiredArgsConstructor;
import org.bedu.CRM.controllers.mappers.ProspectoMapper;
import org.bedu.CRM.model.Prospecto;
import org.bedu.CRM.persistence.ProspectoRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProspectoService {
    private final ProspectoRepository repository;
    private final ProspectoMapper mapper;

    public Prospecto guardaProspecto(Prospecto prospecto) {
        return mapper.prospectoEntityToProspectoModel(
                repository.save(mapper.prospectoModelToProspectoEntity(prospecto))
        );
    }

    public List<Prospecto> obtenProspectos(){
        return repository.findAll().stream().map(prospecto -> mapper.prospectoEntityToProspectoModel(prospecto)).collect(Collectors.toList());
    }

    public Optional<Prospecto> obtenProspecto(long idProspecto) {
        return repository.findById(idProspecto)
                .map(prospecto -> Optional.of(mapper.prospectoEntityToProspectoModel(prospecto)))
                .orElse(Optional.empty());
    }

    public void eliminaProspecto(long idprospecto){
        repository.deleteById(idprospecto);
    }

    public Prospecto actualizaProspecto(Prospecto prospecto){
        return mapper.prospectoEntityToProspectoModel(
                repository.save(mapper.prospectoModelToProspectoEntity(prospecto))
        );
    }

    public long cuenteProspectos(){
        return repository.count();
    }
}
