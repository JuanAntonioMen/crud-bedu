package org.bedu.CRM.persistence.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Table(name = "ETAPAS")
@Entity
@NoArgsConstructor
public class Etapa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long etapaId;

    @Column(nullable = false)
    private String nombre;

    @Column(unique = true, nullable = false)
    private Integer orden;
}
