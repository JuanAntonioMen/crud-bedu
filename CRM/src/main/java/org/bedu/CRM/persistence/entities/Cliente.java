package org.bedu.CRM.persistence.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@Table(name = "CLIENTES")
@Entity
@NoArgsConstructor
public class Cliente {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nombre;

    @Column(nullable = false)
    private String correoContacto;

    @Column(nullable = false)
    private String numeroEmpleados;

    @Column(nullable = false)
    private String direccion;

    @OneToMany
    private List<Etapa> etapas;
}
