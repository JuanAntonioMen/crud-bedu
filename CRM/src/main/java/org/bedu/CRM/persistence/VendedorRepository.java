package org.bedu.CRM.persistence;

import org.bedu.CRM.persistence.entities.Vendedor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendedorRepository extends JpaRepository<Vendedor, Long>{
}
