package org.bedu.CRM.persistence;

import org.bedu.CRM.persistence.entities.Etapa;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EtapaRepository extends JpaRepository<Etapa, Long> {
}
