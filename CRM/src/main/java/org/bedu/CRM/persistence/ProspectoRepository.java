package org.bedu.CRM.persistence;

import org.bedu.CRM.persistence.entities.Prospecto;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProspectoRepository extends JpaRepository<Prospecto, Long>{
}
