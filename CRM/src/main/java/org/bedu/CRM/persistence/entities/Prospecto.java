package org.bedu.CRM.persistence.entities;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@Table(name = "PROSPECTOS")
@Entity
@NoArgsConstructor
public class Prospecto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false)
    private String nombre;

    @Column(nullable = false)
    private String correoContacto;

    @Column(nullable = false)
    private String direccion;
}
