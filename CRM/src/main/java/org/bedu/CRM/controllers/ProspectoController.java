package org.bedu.CRM.controllers;

import lombok.RequiredArgsConstructor;
import org.bedu.CRM.model.Prospecto;
import org.bedu.CRM.services.ProspectoService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/prospecto")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class ProspectoController {

    private final ProspectoService prospectoService;

    @GetMapping("/{prospectoId}")
    public ResponseEntity<Prospecto> getProspecto(@PathVariable Long prospectoId) {

        Optional<Prospecto> prospectoDb = prospectoService.obtenProspecto(prospectoId);

        if (prospectoDb.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El prospecto especificado no existe.");
        }

        return ResponseEntity.ok(prospectoDb.get());
    }

    @GetMapping
    public ResponseEntity<List<Prospecto>> getProspectos() {
        return ResponseEntity.ok(prospectoService.obtenProspectos());
    }

    @PostMapping
    public ResponseEntity<Void> creaProspecto(@Valid @RequestBody Prospecto prospecto) {
        Prospecto prospectoNuevo = prospectoService.guardaProspecto(prospecto);

        return ResponseEntity.created(URI.create(String.valueOf(prospectoNuevo.getId()))).build();
    }

    @PutMapping("/{prospectoId}")
    public ResponseEntity<Void> actualizaProspecto(@PathVariable Long prospectoId, @RequestBody @Valid Prospecto prospecto) {

        prospectoService.actualizaProspecto(prospecto);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/{prospectoId}")
    public ResponseEntity<Void> eliminaProspecto(@PathVariable Long prospectoId) {
        prospectoService.eliminaProspecto(prospectoId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
