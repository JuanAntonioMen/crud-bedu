package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.persistence.entities.Vendedor;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VendedorMapper {

    Vendedor vendedorModelToVendedorEntity(org.bedu.CRM.model.Vendedor vendedorModel);

    org.bedu.CRM.model.Vendedor vendedorEntityToVendedorModel(Vendedor vendedor);
}
