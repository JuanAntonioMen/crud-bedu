package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.model.Cliente;
import org.bedu.CRM.persistence.entities.Visita;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VisitaMapper {

    Visita visitaModelToVisitaEntity(org.bedu.CRM.model.Visita visitaModel);

    org.bedu.CRM.model.Visita visitaEntityToVisitaModel(Visita visita);
}
