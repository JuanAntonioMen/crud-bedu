package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.persistence.entities.Venta;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface VentaMapper {

    Venta ventaModelToVentaEntity(org.bedu.CRM.model.Venta ventaModel);

    org.bedu.CRM.model.Venta ventaEntityToVentaModel(Venta venta);
}
