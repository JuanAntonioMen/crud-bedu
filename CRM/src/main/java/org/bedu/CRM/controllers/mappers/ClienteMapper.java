package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.persistence.entities.Cliente;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ClienteMapper {

    Cliente clienteModelToClienteEntity(org.bedu.CRM.model.Cliente clienteModel);

    org.bedu.CRM.model.Cliente clienteEntityToClienteModel(Cliente cliente);
}
