package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.persistence.entities.Etapa;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EtapaMapper {

    Etapa etapaModelToEtapaEntity(org.bedu.CRM.model.Etapa etapaModel);

    org.bedu.CRM.model.Etapa etapaEntityToEtapaModel(Etapa etapa);
}
