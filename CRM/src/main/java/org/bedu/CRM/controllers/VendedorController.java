package org.bedu.CRM.controllers;

import lombok.RequiredArgsConstructor;
import org.bedu.CRM.model.Vendedor;
import org.bedu.CRM.services.VendedorService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.net.URI;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/vendedor")
@CrossOrigin(origins = "*")
@RequiredArgsConstructor
public class VendedorController {

    private final VendedorService vendedorService;

    @GetMapping("/{vendedorId}")
    public ResponseEntity<Vendedor> getVendedor(@PathVariable Long vendedorId) {

        Optional<Vendedor> vendedorDb = vendedorService.obtenVendedor(vendedorId);

        if (vendedorDb.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "El vendedor especificado no existe.");
        }

        return ResponseEntity.ok(vendedorDb.get());
    }

    @GetMapping
    public ResponseEntity<List<Vendedor>> getVendedores() {
        return ResponseEntity.ok(vendedorService.obtenVendedores());
    }

    @PostMapping
    public ResponseEntity<Void> creaVendedor(@Valid @RequestBody Vendedor vendedor) {
        Vendedor vendedorNuevo = vendedorService.guardaVendedor(vendedor);

        return ResponseEntity.created(URI.create(String.valueOf(vendedorNuevo.getId()))).build();
    }

    @PutMapping("/{vendedorId}")
    public ResponseEntity<Void> actualizaVendedor(@PathVariable Long vendedorId, @RequestBody @Valid Vendedor vendedor) {

        vendedorService.actualizaVendedor(vendedor);

        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @DeleteMapping("/{vendedorId}")
    public ResponseEntity<Void> eliminaVendedor(@PathVariable Long vendedorId) {
        vendedorService.eliminaVendedor(vendedorId);
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }
}
