package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.persistence.entities.Producto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProductoMapper {

    Producto productoModelToProductoEntity(org.bedu.CRM.model.Producto productoModel);

    org.bedu.CRM.model.Producto productoEntityToProductoModel(Producto producto);

}
