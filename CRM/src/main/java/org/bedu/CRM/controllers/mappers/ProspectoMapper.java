package org.bedu.CRM.controllers.mappers;

import org.bedu.CRM.persistence.entities.Prospecto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface ProspectoMapper {

    Prospecto prospectoModelToProspectoEntity(org.bedu.CRM.model.Prospecto prospectoModel);

    org.bedu.CRM.model.Prospecto prospectoEntityToProspectoModel(Prospecto prospecto);
}
