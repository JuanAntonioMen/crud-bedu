package org.bedu.CRM.model;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@Builder
public class Producto {

    @PositiveOrZero(message = "El id del producto no puede ser un número negativo")
    private long id;

    @NotBlank(message = "El nombre del producto no puede estar en blanco.")
    @Size(min = 4, max = 255, message = "El nombre del producto debe tener entre 4 y 255 letras.")
    private String nombre;

    @NotBlank(message = "La categoria del producto no puede estar en blanco.")
    private String categoria;

    @DecimalMin(value = "1.00", inclusive = true, message = "El precio del producto debe ser de al menos 1.00")
    private float precio;

    @NotEmpty(message = "El número de registro del producto no puede estar en blanco")
    @Pattern(regexp = "^(\\d{3}[-]?){2}\\d{4}$", message="El número de registro debe ser con el formato XXX-XXX-XXXX.")
    private String numeroRegistro;

    @PastOrPresent(message = "La fecha de creación del producto no puede ser mayor a la fecha actual")
    private
    LocalDate fechaCreacion;

}
