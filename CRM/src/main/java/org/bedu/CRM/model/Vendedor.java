package org.bedu.CRM.model;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@Builder
public class Vendedor {

    @PositiveOrZero(message = "El id del vendedor no puede ser un número negativo")
    private long id;

    @NotBlank(message = "El nombre del vendedor no puede estar vacío")
    @Size(min = 5, max = 255, message = "El nombre del vendedor debe tener al menos 5 letras y ser menor a 255")
    private String nombre;

    @Email(message = "Debe ingresar una direccion de correo valida")
    private String correo;

    @NotBlank(message = "Se debe proporcionar una dirección")
    private String direccion;

    @NotBlank(message = "Se debe proporcionar el numero telefonico del vendedor")
    private String telefono;
}
