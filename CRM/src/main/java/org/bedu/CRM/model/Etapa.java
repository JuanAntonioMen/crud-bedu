package org.bedu.CRM.model;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

import javax.validation.constraints.*;

@Data
@Builder
public class Etapa {

    @PositiveOrZero(message = "El id de la etapa no puede ser un numero negativo")
    private long etapaId;

    @NotBlank(message = "El nombre de la etapa no puede estar en vacio.")
    @Size(min = 4, max = 255, message = "El nombre de la etapa debe ser mayor a 4 letras y menor de 255 letras.")
    private String nombre;

    @Positive(message = "El orden de la etapa debe ser un numero positivo mayor a 0")
    private int orden;
}
