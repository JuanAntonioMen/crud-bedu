package org.bedu.CRM.model;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;
import java.util.List;

@Data
@Builder
public class Cliente {

    @PositiveOrZero(message = "El id del cliente no puede ser un número negativo")
    private long id;

    @NotBlank(message = "El nombre del cliente no puede estar vacío")
    @Size(min = 5, max = 255, message = "El nombre del cliente debe tener al menos 5 letras y ser menor a 255")
    private String nombre;

    @Email(message = "Debe ingresar una direccion de correo valida")
    private String correoContacto;

    @Min(value = 10, message = "Los clientes con menos de 10 empleados no son válidos")
    @Max(value = 10000, message = "Los clientes con más de 10000 empleados no son válidos")
    private String numeroEmpleados;

    @NotBlank(message = "Se debe proporcionar una dirección")
    private String direccion;

    private List<Etapa> etapas;
}
