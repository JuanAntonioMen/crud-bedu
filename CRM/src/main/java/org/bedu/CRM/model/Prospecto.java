package org.bedu.CRM.model;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@Builder
public class Prospecto {

    @PositiveOrZero(message = "El id del prospecto no puede ser un número negativo")
    private long id;

    @NotBlank(message = "El nombre del prospecto no puede estar vacío")
    @Size(min = 5, max = 255, message = "El nombre del prospecto debe tener al menos 5 letras y ser menor a 255")
    private String nombre;

    @Email(message = "Debe ingresar una direccion de correo valida")
    private String correoContacto;

    @NotBlank(message = "Se debe proporcionar una dirección")
    private String direccion;
}
