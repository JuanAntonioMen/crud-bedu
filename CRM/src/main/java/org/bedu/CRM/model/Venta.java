package org.bedu.CRM.model;

import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.LocalDateTime;
import java.util.List;

@Data
@Builder
public class Venta {
    @PositiveOrZero(message = "El identificador de la venta no puede ser un número negativo")
    private long ventaId;

    @DecimalMin(value = "1.00", inclusive = true, message = "La venta debe ser de al menos 1.00")
    private float monto;

    @NotEmpty(message = "La venta debe tener por lo menos un producto.")
    private List<Producto> productos;

    @NotNull(message = "El cliente de la venta es requerido.")
    private Cliente cliente;

    @PastOrPresent(message = "La fecha de la venta no debe ser mayor a la fecha actual.")
    private LocalDateTime fechaCreacion;

}
